import uvicorn
from fastapi import FastAPI
from fastapi import APIRouter
from fastapi import Response, status
from starlette.middleware.cors import CORSMiddleware

route = APIRouter()


@route.get("/hello")
def home():
    return Response("Hello Ci/CD")


def create_app() -> FastAPI:
    app = FastAPI()
    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
    app.include_router(route)
    return app

app = create_app()


if __name__ == '__main__':
    uvicorn.run(app, host="0.0.0.0", port=8080)
    